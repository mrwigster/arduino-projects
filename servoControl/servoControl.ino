// servoControl.ino
#include <Servo.h>
Servo myServo;

int const potPin = A0;
int potVal;
int angle;
void setup() {
	myServo.attach(9);
	Serial.begin(9600);
	pinMode(13, OUTPUT);
}

void loop() {
	potVal = analogRead(potPin);
	Serial.print("potVal: ");
	Serial.print(potVal);
	angle = map(potVal, 0, 1023, 0, 179);
	if (angle > 50 && angle <100) {
		digitalWrite(13, HIGH);
	}
	else {
		digitalWrite(13, LOW);
	}
	Serial.print(", angle: ");
	Serial.println(angle);
	myServo.write(angle);
	delay(1);
}

